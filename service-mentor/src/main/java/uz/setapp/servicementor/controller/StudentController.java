package uz.setapp.servicementor.controller;

import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqStudent;
import uz.setapp.servicementor.payload.ReqStudentMergeCertificate;
import uz.setapp.servicementor.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("/add")
    public HttpEntity<?> addStudent(@RequestBody ReqStudent reqStudent ){
        Response response = studentService.save(reqStudent);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/allStudent")
    public HttpEntity<?> getAllStudent(){
        Response response = studentService.getAll();

        return ResponseEntity.ok(response);
    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> updateStudent(@PathVariable Long id,@RequestBody ReqStudent reqStudent){
        Response response = studentService.update(id,reqStudent);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/delete/{id}")
    public HttpEntity<?> deleteStudent(@PathVariable Long id){
        Response response = studentService.delete(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/studentCertificate/add")
    public HttpEntity<?> addStudentCertificate(@RequestBody ReqStudentMergeCertificate reqStudentMergeCertificate){
        Response response = studentService.addStudentCertificate(reqStudentMergeCertificate);
        return ResponseEntity.ok(response);
    }
}
