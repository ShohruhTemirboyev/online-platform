package uz.setapp.servicementor.controller;

import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqCertificate;
import uz.setapp.servicementor.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/certificate")
public class CertificateController {

    @Autowired
    CertificateService certificateService;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody ReqCertificate reqCertificate){
        Response response = certificateService.save(reqCertificate);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getAll")
    public HttpEntity<?> getAllCertificate(){
        Response response = certificateService.getAllCertificate();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get/{id}")
    public HttpEntity<?> getCertificate(@PathVariable Long id){
        Response response = certificateService.getCertificate(id);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> update(@PathVariable Long id, @RequestBody ReqCertificate reqCertificate){
        Response response = certificateService.update(id,reqCertificate);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Long id){
        Response response = certificateService.delete(id);
        return ResponseEntity.ok(response);
    }
}
