package uz.setapp.servicementor.controller;

import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqMentor;
import uz.setapp.servicementor.service.MentorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/mentor")
public class MentorController {

    @Autowired
    MentorService mentorService;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody ReqMentor reqMentor){
        Response response = mentorService.save(reqMentor);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> update(@PathVariable Long id,@RequestBody ReqMentor reqMentor){
        Response response = mentorService.update(id,reqMentor);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/list")
    public HttpEntity<?> list(){
        Response response = mentorService.list();
        return ResponseEntity.ok(response);
    }

    @PostMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable Long id){
        Response response = mentorService.delete(id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/find/{mentorId}")
    public HttpEntity<?> findMentor(@PathVariable Long mentorId){
        return ResponseEntity.ok(mentorService.findMentor(mentorId));
    }
}
