package uz.setapp.servicementor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceMentorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceMentorApplication.class, args);
	}

}
