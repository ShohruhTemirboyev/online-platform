package uz.setapp.servicementor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqStudent {
    private Long id;
    private Long userId;
    private Double deposit;
    private Double price;
}
