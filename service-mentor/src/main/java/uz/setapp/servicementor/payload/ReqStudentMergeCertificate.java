package uz.setapp.servicementor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqStudentMergeCertificate {
    private Long studentId;
    private List<Long> certificatesId;
}
