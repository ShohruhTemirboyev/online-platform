package uz.setapp.servicementor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqCertificate {
    private Long id;
    private String name;
    private String description;
    private Long attachmentId;
}
