package uz.setapp.servicementor.entity.enums;


public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_SUPER_ADMIN
}
