package uz.setapp.servicementor.entity;

import uz.setapp.servicementor.entity.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class Role {

private Long id;

private RoleEnum roleEnum;

}
