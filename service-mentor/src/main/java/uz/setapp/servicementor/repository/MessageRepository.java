package uz.setapp.servicementor.repository;

import uz.setapp.servicementor.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    Message findByCode(Integer code);
}
