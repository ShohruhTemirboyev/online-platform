package uz.setapp.servicementor.service;

import uz.setapp.servicementor.entity.Certificate;
import uz.setapp.servicementor.entity.Student;
import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqStudent;
import uz.setapp.servicementor.payload.ReqStudentMergeCertificate;
import uz.setapp.servicementor.repository.CertificateRepository;
import uz.setapp.servicementor.repository.MessageRepository;
import uz.setapp.servicementor.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    MessageRepository messageRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CertificateRepository certificateRepository;


    RestTemplate restTemplate = new RestTemplate();
    String userUrl = "http://localhost:2000/users/api/auth/findUser/";


    public Response save(ReqStudent reqStudent) {
        Response response = new Response();

        if (Boolean.TRUE.equals(restTemplate.getForObject(userUrl + reqStudent.getUserId().toString(), Boolean.class))){
            Student student = new Student();
//            List<Certificate> certificates = new ArrayList<>();
//            student.setUser(reqStudent.getUserId());
            student.setUserId(reqStudent.getUserId());
            student.setDeposit(reqStudent.getDeposit());
            student.setPrice(reqStudent.getPrice());
            Student save = studentRepository.save(student);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(101));
        }
        return response;
    }

    public Response getAll() {
        Response response = new Response();
        try {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from app_student s where s.active=true");
            response.setMessage(messageRepository.findByCode(0));
            response.setData(maps);
        } catch (Exception e) {
            response.setMessage(messageRepository.findByCode(1));
        }
        return response;

    }

    public Response update(Long id,ReqStudent reqStudent) {
        Response response = new Response();
        Optional<Student> byId = studentRepository.findById(id);
        if (byId.isPresent()) {
            Student student = byId.get();
            if (Boolean.TRUE.equals(restTemplate.getForObject(userUrl + reqStudent.getUserId().toString(), Boolean.class))) {
                student.setUserId(reqStudent.getUserId());
                student.setDeposit(reqStudent.getDeposit());
                student.setPrice(reqStudent.getPrice());
                Student save = studentRepository.save(student);
                response.setData(save.getId());
                response.setMessage(messageRepository.findByCode(0));
            }else {
                response.setMessage(messageRepository.findByCode(101));
            }
        } else {
            response.setMessage(messageRepository.findByCode(103));
        }
        return response;
    }

    public Response delete(Long id) {
        Response response = new Response();
        Optional<Student> byId = studentRepository.findById(id);
        if (byId.isPresent()) {
            Student student = byId.get();
            student.setActive(false);
            studentRepository.save(student);
            response.setMessage(messageRepository.findByCode(0));
        } else {
            response.setMessage(messageRepository.findByCode(103));
        }
        return response;
    }

    public Response addStudentCertificate(ReqStudentMergeCertificate reqStudentMergeCertificate){
        Response response = new Response();
        Optional<Student> exist = studentRepository.findById(reqStudentMergeCertificate.getStudentId());
        List<Certificate> certificates = new ArrayList<>();
        if (exist.isPresent()){
            if (reqStudentMergeCertificate.getCertificatesId() != null) {
                    for (int i = 0; i < reqStudentMergeCertificate.getCertificatesId().size(); i++) {
                        Optional<Certificate> byId = certificateRepository.findById(reqStudentMergeCertificate.getCertificatesId().get(i));
                        if (byId.isPresent()){
                            certificates.add(byId.get());
                        }else {
                            response.setMessage(messageRepository.findByCode(102));
                            return response;
                        }
                    }
                    Student student = exist.get();
                    student.setCertificates(certificates);
                    studentRepository.save(student);
                }
            response.setMessage(messageRepository.findByCode(0));

        }else {
            response.setMessage(messageRepository.findByCode(103));
        }
        return response;
    }
}
