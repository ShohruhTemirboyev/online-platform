package uz.setapp.servicementor.service;

import uz.setapp.servicementor.config.SecurityConfig;
import uz.setapp.servicementor.entity.Certificate;
import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqCertificate;
import uz.setapp.servicementor.repository.CertificateRepository;
import uz.setapp.servicementor.repository.MessageRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CertificateService {

    @Autowired
    CertificateRepository certificateRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    SecurityConfig securityConfig;
    RestTemplate restTemplate = new RestTemplate();

    String attachmentUrl = "http://localhost:8088/attachment/api/attachment/find/";
    public Response save(ReqCertificate reqCertificate) {
        Response response = new Response();
        String token = securityConfig.requestUrl().getHeader("token");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+token);
        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        ResponseEntity<Boolean> exchange = restTemplate.exchange(attachmentUrl + reqCertificate.getAttachmentId(), HttpMethod.GET, entity, boolean.class);
        if (Boolean.TRUE.equals(exchange.getBody())) {
            Certificate certificate = new Certificate();
            certificate.setName(reqCertificate.getName());
            certificate.setDescription(reqCertificate.getDescription());
            certificate.setAttachmentId(reqCertificate.getAttachmentId());
            Certificate save = certificateRepository.save(certificate);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(1006));
        }


        return response;
    }

    public Response getAllCertificate() {
        Response response = new Response();
        try {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from certificate c where c.active=true");
            response.setMessage(messageRepository.findByCode(0));
            response.setData(maps);
        } catch (Exception e) {
            response.setMessage(messageRepository.findByCode(1));
        }
        return response;
    }

    public Response getCertificate(Long id) {
        Response response = new Response();
        Optional<Certificate> byId = certificateRepository.findById(id);

        if (byId.isPresent()) {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from certificate c where c.active=true and c.id = ?", id);
            response.setMessage(messageRepository.findByCode(0));
            response.setData(maps);
        }else {
            response.setMessage(messageRepository.findByCode(102));
        }
        return response;
    }

    public Response update(Long id,ReqCertificate reqCertificate){
        Response response = new Response();
        Optional<Certificate> byId = certificateRepository.findById(id);
        if (byId.isPresent()){
            Certificate certificate = byId.get();
            certificate.setName(reqCertificate.getName());
            certificateRepository.save(certificate);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(102));
        }
        return response;
    }

    public Response delete(@NotNull Long id){
        Response response = new Response();
        Optional<Certificate> byId = certificateRepository.findById(id);
        if (byId.isPresent()){
            Certificate certificate = byId.get();
            certificate.setActive(false);
            Certificate save = certificateRepository.save(certificate);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(102));
        }
        return response;
    }
}
