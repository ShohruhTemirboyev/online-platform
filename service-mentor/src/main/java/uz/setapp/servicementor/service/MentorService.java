package uz.setapp.servicementor.service;

import uz.setapp.servicementor.entity.Mentor;
import uz.setapp.servicementor.entity.models.Response;
import uz.setapp.servicementor.payload.ReqMentor;
import uz.setapp.servicementor.repository.MentorRepository;
import uz.setapp.servicementor.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MentorService {

    @Autowired
    MentorRepository mentorRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    RestTemplate restTemplate = new RestTemplate();
    String userUrl = "http://localhost:8088/users/api/auth/findUser/";


    public Response save(ReqMentor reqMentor){
        Response response = new Response();

        if (Boolean.TRUE.equals(restTemplate.getForObject(userUrl + reqMentor.getUserId().toString(), Boolean.class))){
            Mentor mentor = new Mentor();
            mentor.setUserId(reqMentor.getUserId());

            Mentor save = mentorRepository.save(mentor);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }
        else {
            response.setMessage(messageRepository.findByCode(101));
        }
        return response;
    }

    public Response update(Long id,ReqMentor reqMentor){
        Response response = new Response();
        Optional<Mentor> byId = mentorRepository.findById(id);

        if (byId.isPresent()){
            Mentor mentor = byId.get();
            if (Boolean.TRUE.equals(restTemplate.getForObject(userUrl + reqMentor.getUserId().toString(), Boolean.class))) {
                mentor.setUserId(reqMentor.getUserId());
                mentorRepository.save(mentor);
                response.setMessage(messageRepository.findByCode(0));
            }else {
                response.setMessage(messageRepository.findByCode(101));
            }
        }else {
            response.setMessage(messageRepository.findByCode(104));

        }
        return response;
    }

    public Response list(){
        Response response = new Response();
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from mentor m where m.active=true");
        response.setData(maps);
        response.setMessage(messageRepository.findByCode(0));
        return response;
    }

    public Response delete(Long id){
        Response response = new Response();
        Optional<Mentor> byId = mentorRepository.findById(id);
        if (byId.isPresent()){
            Mentor mentor = byId.get();
            mentor.setActive(false);
            Mentor save = mentorRepository.save(mentor);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(104));
        }
        return response;
    }

    public boolean findMentor(Long mentorId){
        Optional<Mentor> byId = mentorRepository.findById(mentorId);
        return byId.isPresent();
    }
}

