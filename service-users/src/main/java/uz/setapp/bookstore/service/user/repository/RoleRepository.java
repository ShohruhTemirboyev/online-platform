package uz.setapp.bookstore.service.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.bookstore.service.user.entity.Role;
import uz.setapp.bookstore.service.user.entity.enums.RoleName;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByRoleName(RoleName roleName);
    Optional<Role> findById(Long roleId);
}
