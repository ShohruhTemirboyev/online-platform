package uz.setapp.bookstore.service.user.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.setapp.bookstore.service.user.entity.Role;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResUser {
    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private Integer language;
    private List<Role> role;

}
