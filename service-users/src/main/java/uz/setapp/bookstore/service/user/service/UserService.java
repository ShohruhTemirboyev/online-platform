package uz.setapp.bookstore.service.user.service;

import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import uz.setapp.bookstore.service.user.config.SecurityConfig;
import uz.setapp.bookstore.service.user.entity.User;
import uz.setapp.bookstore.service.user.entity.UserDto;
import uz.setapp.bookstore.service.user.entity.Role;
import uz.setapp.bookstore.service.user.entity.enums.RoleName;
import uz.setapp.bookstore.service.user.entity.models.Response;
import uz.setapp.bookstore.service.user.exceptions.AppException;
import uz.setapp.bookstore.service.user.payload.*;
import uz.setapp.bookstore.service.user.repository.MessageRepository;
import uz.setapp.bookstore.service.user.repository.RoleRepository;
import uz.setapp.bookstore.service.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.setapp.bookstore.service.user.security.JwtTokenProvider;

//@RequiredArgsConstructor
@Service
public class UserService implements UserDetailsService {


	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final RoleRepository roleRepository;
	private final MessageRepository messageRepository;
	private final ObjectMapper objectMapper;
	private final JdbcTemplate jdbcTemplate;
	private final AuthenticationManager authenticationManager;
	private final JwtTokenProvider jwtTokenProvider;
	private final SecurityConfig securityConfig;

	public UserService(UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder, RoleRepository roleRepository, MessageRepository messageRepository, ObjectMapper objectMapper, JdbcTemplate jdbcTemplate,@Lazy AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider,@Lazy SecurityConfig securityConfig) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.roleRepository = roleRepository;
		this.messageRepository = messageRepository;
		this.objectMapper = objectMapper;
		this.jdbcTemplate = jdbcTemplate;
		this.authenticationManager = authenticationManager;
		this.jwtTokenProvider = jwtTokenProvider;
		this.securityConfig = securityConfig;
	}

	public Response saveUser(ReqUser reqUser) {
		Response response = new Response();
		try {
			if (!userRepository.existsByPhone(reqUser.getPhone())) {
				User user = new User();
				user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
				user.setPhone(reqUser.getPhone());
				user.setFirstName(reqUser.getFirstName());
				user.setLastName(reqUser.getLastName());
				user.setEmail(reqUser.getEmail());
				user.setActive(true);
				user.setLastActive(true);
				user.setRoles((Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_USER))));
				User save = userRepository.save(user);
				response.setMessage(messageRepository.findByCode(0));
				response.setData(save.getId());

			} else {
				response.setMessage(messageRepository.findByCode(102));
			}
		} catch (Exception ex) {
			System.out.println(ex);
			response.setMessage(messageRepository.findByCode(1));
		}
		return response;
	}

	public Response loginUser(String phone, String password) {
		Response response = new Response();
		try {
			Optional<User> userOptional = userRepository.findByPhone(phone);
			if (userOptional.isPresent()) {
				if (passwordEncoder.matches(password, userOptional.get().getPassword())) {
					JwtResponse jwtResponse = getJwtRespons(phone, password);
					jwtResponse.setUserId(userOptional.get().getId());
					response.setData(jwtResponse);
					response.setMessage(messageRepository.findByCode(0));
				} else {
					response.setMessage(messageRepository.findByCode(103));
				}
			} else {
				response.setMessage(messageRepository.findByCode(101));
			}
		} catch (Exception e) {
			response.setMessage(messageRepository.findByCode(1));
		}
		return response;
	}

	public JwtResponse getJwtRespons(String phoneNumber, String password) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(phoneNumber, password));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return new JwtResponse(jwtTokenProvider.generateToken(authentication));
	}

	@Override
	public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
		return userRepository.findByPhone(phone).get();
	}

	public UserDetails loadUserByUserId(Long id) {
		return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
	}


	@Value("DevSupperKey")
	private String secretKey;

//    @PostConstruct
//    protected void init() {
//        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
//    }

	public UserDto validateToken(String token) {
		try {

			String login = Jwts.parser()
					.setSigningKey(secretKey)
					.parseClaimsJws(token)
					.getBody()
					.getSubject();
			Optional<User> userOptional = userRepository.findById(Long.valueOf(login));
			if (userOptional.isEmpty()) {
				throw new AppException("User not found", HttpStatus.NOT_FOUND);
			}

			User user = userOptional.get();
			UserDto userDto = new UserDto();
			userDto.setId(user.getId());
			userDto.setLogin(user.getPhone());
			userDto.setToken(token);
			StringBuilder roleList = new StringBuilder();
			List<Role> roles = user.getRoles();
			for (int i = 0; i < roles.size(); i++) {
				if (roles.size() - i == 1) {
					roleList.append(roles.get(i).getRoleName());
				} else {
					roleList.append(roles.get(i).getRoleName()).append(",");
				}
			}
			userDto.setRoleList(roleList.toString());
			return userDto;
		} catch (Exception e) {
			throw new AppException("User not found", HttpStatus.UNAUTHORIZED);
		}
	}

	public Response updateUser(ReqUser reqUser, Long id) {
		Response response = new Response();
		Optional<User> user1 = userRepository.findById(id);
		Optional<User> byPhoneAndActive = userRepository.findByPhone(reqUser.getPhone());
		if (user1.isPresent()) {
			if (!byPhoneAndActive.isPresent() || byPhoneAndActive.get().getPhone().equals(user1.get().getPhone())) {
				User user = user1.get();
				user.setPhone(reqUser.getPhone());
				user.setFirstName(reqUser.getFirstName());
				user.setLastName(reqUser.getLastName());
				user.setEmail(reqUser.getEmail());
				User save = userRepository.save(user);
				response.setData(save.getId());
				response.setMessage(messageRepository.findByCode(0));
			} else {
				response.setMessage(messageRepository.findByCode(102));
			}
		} else {
			response.setMessage(messageRepository.findByCode(101));
		}
		return response;
	}

	public Response listUsers(Integer page) {
		Response response = new Response();
		ObjectNode objectNode = objectMapper.createObjectNode();
		if (page != null) {
			Integer count = jdbcTemplate.queryForObject("select count(id) from app_user au where active = true", Integer.class);
			int a = 20 * page;
			List<Map<String, Object>> maps = jdbcTemplate.queryForList("select au.id, au.first_name, au.last_name, au.email, au.language, au.phone from app_user au where au.active=true offset ? limit 20", a);
			for (int i = 0; i < maps.size(); i++) {
				Long id = (Long) maps.get(i).get("id");
				List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.role_name from role r left join user_role ur on r.id = ur.role_id left join app_user au on au.id = ur.user_id where au.active=true and ur.user_id = ?", id);
				maps.get(i).put("roles", maps1);
			}
			objectNode.put("count", count);
			objectNode.putPOJO("data", maps);
			response.setData(objectNode);
			response.setMessage(messageRepository.findByCode(0));
		} else {
			List<Map<String, Object>> maps = jdbcTemplate.queryForList("select au.id, au.first_name, au.last_name, au.email, au.language, au.phone from app_user au where au.active=true");
			for (int i = 0; i < maps.size(); i++) {
				Long id = (Long) maps.get(i).get("id");
				List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.role_name from role r left join user_role ur on r.id = ur.role_id left join app_user au on au.id = ur.user_id where au.active=true and ur.user_id = ?", id);
				maps.get(i).put("roles", maps1);
			}
			response.setData(maps);
			response.setMessage(messageRepository.findByCode(0));
		}
		return response;
	}

	public Response deleteUser(Long id) {
		Response response = new Response();
		Optional<User> byId = userRepository.findById(id);
		if (byId.isPresent()) {
			User user = byId.get();
			user.setActive(false);
			userRepository.save(user);
			response.setMessage(messageRepository.findByCode(0));
		} else {
			response.setMessage(messageRepository.findByCode(101));
		}
		return response;
	}

	public Response updatePassword(String currentPass, String newPass) {
		Response response = new Response();
		User currentUser = securityConfig.getCurrentUser();
		String password = currentUser.getPassword();
//        String encode = passwordEncoder.encode(currentPass);
//        String encode2 = passwordEncoder.encode(currentPass);
		if (passwordEncoder.matches(currentPass, password)) {
			if (passwordEncoder.matches(newPass, password)) {
				response.setMessage(messageRepository.findByCode(1));
			} else {
				currentUser.setPassword(passwordEncoder.encode(newPass));
				userRepository.save(currentUser);
				response.setMessage(messageRepository.findByCode(0));
			}
		} else {
			response.setMessage(messageRepository.findByCode(1));
		}
		return response;
	}

	public Response filterUser(ReqUserFilter reqUserFilter) {
		Response response = new Response();
		String sql = "";
		if (reqUserFilter.getFirstName() != null) {
			sql += " and (upper(au.first_name) like '%" + reqUserFilter.getFirstName().toUpperCase() + "%')";
		}
		if (reqUserFilter.getLastName() != null) {
			sql += " and (upper(au.last_name) like '%" + reqUserFilter.getLastName().toUpperCase() + "%')";
		}
		if (reqUserFilter.getPhone() != null) {
			sql += " and (upper(au.phone) like '%" + reqUserFilter.getPhone().toUpperCase() + "%')";
		}
		if (reqUserFilter.getEmail() != null) {
			sql += " and (upper(au.email) like '%" + reqUserFilter.getEmail().toUpperCase() + "%')";
		}
		if (reqUserFilter.getLanguage() != null) {
			sql += " and au.language = '" + reqUserFilter.getLanguage() + "'";
		}
//        if (reqFilter.getRoleId() != null){
//            sql+=" and au.roleId = '" + reqFilter.getRoleId() + "'";
//        }
//
//        if (reqFilter.getRestaurantId() != null){
//            sql+=" and au.restaurantId = '" + reqFilter.getRestaurantId() + "'";
//        }

		if (sql.length() == 0 && reqUserFilter.getRolesId() == null) {
			List<Map<String, Object>> maps = jdbcTemplate.queryForList("select au.id, au.first_name, au.last_name, au.email, au.language, au.phone from app_user au where au.active=true " + sql);
			for (int i = 0; i < maps.size(); i++) {
				Long id = (Long) maps.get(i).get("id");
				List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from role r left join user_role ur on r.id = ur.role_id left join app_user au on au.id = ur.user_id where au.active=true and ur.user_id = ? " + sql, id);
				maps.get(i).put("roles", maps1);
			}
			response.setData(maps);
		} else {
			List<Map<String, Object>> list = new ArrayList<>();
			List<Map<String, Object>> maps = jdbcTemplate.queryForList("select au.id, au.first_name, au.last_name, au.email, au.language, au.phone from app_user au where au.active=true " + sql);
			for (int i = 0; i < maps.size(); i++) {
				Long id = (Long) maps.get(i).get("id");
				List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from role r left join user_role ur on r.id = ur.role_id left join app_user au on au.id = ur.user_id where au.active=true and ur.user_id = ? " + sql, id);
				if (reqUserFilter.getRolesId() != null) {
					for (int n = 0; n < maps1.size(); n++) {
						for (int i2 = 0; i2 < reqUserFilter.getRolesId().size(); i2++) {
							Long id1 = (Long) maps1.get(n).get("id");
							Long id2 = reqUserFilter.getRolesId().get(i2);
							if (id1 == id2) {
								maps.get(i).put("roles", maps1);
								list.add(maps.get(i));
								break;
							}
						}
					}
				} else {
					maps.get(i).put("roles", maps1);
					list.add(maps.get(i));
				}
			}
			response.setData(list);
		}
		response.setMessage(messageRepository.findByCode(0));
		return response;
	}

	public boolean user(Long id) {
		Optional<User> byId = userRepository.findById(id);
		return byId.isPresent();
	}
}


