package uz.setapp.bookstore.service.user.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUserFilter {
    private String firstName;
    private String lastName;
    private String email;
    private Integer language;
    private String phone;
    private String role;
    private List<Long> rolesId;
}
