package uz.setapp.bookstore.service.user.payload;

import lombok.Data;

@Data
public class ReqUser {

    private String password;
    private String phone;
    private String firstName;
    private String lastName;
    private String email;

}
