package uz.setapp.bookstore.service.user.entity.enums;

import javax.persistence.Enumerated;

 public  enum  RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_SUPER_ADMIN
}
