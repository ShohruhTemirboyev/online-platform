package uz.setapp.bookstore.service.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.bookstore.service.user.config.SecurityConfig;
import uz.setapp.bookstore.service.user.entity.User;
import uz.setapp.bookstore.service.user.entity.UserDto;
import uz.setapp.bookstore.service.user.entity.models.Response;
import uz.setapp.bookstore.service.user.payload.*;
import uz.setapp.bookstore.service.user.repository.MessageRepository;
import uz.setapp.bookstore.service.user.service.UserService;

@RestController
@RequestMapping("/api/auth")
@Slf4j
public class UserControllers {

    @Autowired
    UserService userService;
    @Autowired
    SecurityConfig securityConfig;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    MessageRepository messageRepository;

    @PostMapping("/register")
    public HttpEntity<?> addUser(@RequestBody ReqUser reqUser) {
        Response response = userService.saveUser(reqUser);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqLogin reqLogin) {
        Response apiResponseModel = userService.loginUser(reqLogin.getPhone(), reqLogin.getPassword());
        return ResponseEntity.ok(apiResponseModel);
    }

    @GetMapping("/getUser")
    public HttpEntity<?> getUser(@RequestParam Long userId) {
        return ResponseEntity.ok(userService.loadUserByUserId(userId));
    }


    @GetMapping("/validateToken")
    public ResponseEntity<UserDto> signIn(@RequestParam String token) {
        return ResponseEntity.ok(userService.validateToken(token));
    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> updateUser(@RequestBody ReqUser reqUser, @PathVariable Long id) {
        Response response = userService.updateUser(reqUser, id);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/updatePass")
    public HttpEntity<?> updatePass(@RequestParam String currentPass, @RequestParam String newPass) {
        Response response = userService.updatePassword(currentPass, newPass);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/data")
    public HttpEntity<?> getUserData() {
        Response response = new Response();
        ObjectNode data = objectMapper.createObjectNode();
        User currentUser = securityConfig.getCurrentUser();
        data.put("id", currentUser.getId());
        data.put("phone", currentUser.getPhone());
        data.put("firstName", currentUser.getFirstName());
        data.put("lastName", currentUser.getLastName());
        data.put("email", currentUser.getEmail());
        data.put("language", currentUser.getLanguage());
        data.putPOJO("roles", currentUser.getRoles());
        response.setMessage(messageRepository.findByCode(0));
        response.setData(data);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/list")
    public HttpEntity<?> getUserList(@RequestParam(required = false) Integer page){
        Response response = userService.listUsers(page);
        return ResponseEntity.ok(response);
    }
    @PostMapping("/delete/{id}")
    public HttpEntity<?> deleteUser(@PathVariable Long id) {
        Response response = userService.deleteUser(id);
        return ResponseEntity.ok(response);
    }
    @PostMapping("/filter")
    public HttpEntity<?> filterUser(@RequestBody ReqUserFilter reqUserFilter) {
        Response response = userService.filterUser(reqUserFilter);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/findUser/{id}")
    public HttpEntity<?> findUser(@PathVariable Long id){
        return ResponseEntity.ok(userService.user(id));
    }
}
