package uz.setapp.bookstore.service.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.bookstore.service.user.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {

    Message findByCode(Integer code);
}
