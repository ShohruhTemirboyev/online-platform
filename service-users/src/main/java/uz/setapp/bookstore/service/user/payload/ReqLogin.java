package uz.setapp.bookstore.service.user.payload;

import lombok.Data;

@Data
public class ReqLogin {
    private String phone;
    private String password;
}
