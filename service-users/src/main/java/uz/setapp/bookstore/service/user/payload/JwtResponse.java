package uz.setapp.bookstore.service.user.payload;

import lombok.Data;

@Data
public class JwtResponse {
    private Long userId;
    private String token;
    private String tokenType="Bearer";

    public JwtResponse(String token) {
        this.token = token;
    }
}
