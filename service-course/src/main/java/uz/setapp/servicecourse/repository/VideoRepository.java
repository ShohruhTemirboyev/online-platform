package uz.setapp.servicecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicecourse.entity.Video;
@Repository
public interface VideoRepository extends JpaRepository<Video,Long> {
}
