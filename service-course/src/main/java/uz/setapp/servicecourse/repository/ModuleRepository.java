package uz.setapp.servicecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicecourse.entity.Module;

@Repository
public interface ModuleRepository extends JpaRepository<Module,Long> {
}
