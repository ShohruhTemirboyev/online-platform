package uz.setapp.servicecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicecourse.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {

    Message findByCode(Integer code);
}
