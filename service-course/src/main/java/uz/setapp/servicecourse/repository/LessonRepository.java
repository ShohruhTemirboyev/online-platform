package uz.setapp.servicecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicecourse.entity.Lesson;

@Repository
public interface LessonRepository extends JpaRepository<Lesson,Long> {
}
