package uz.setapp.servicecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicecourse.entity.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating,Long> {
}
