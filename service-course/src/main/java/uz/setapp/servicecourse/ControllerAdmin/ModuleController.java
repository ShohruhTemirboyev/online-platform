package uz.setapp.servicecourse.ControllerAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.payload.ReqModule;
import uz.setapp.servicecourse.serviceAdmin.ModuleService;

@RestController
@RequestMapping("/admin/module")
public class ModuleController {
    private final ModuleService moduleService;
    @Autowired
    public ModuleController(ModuleService moduleService) {
        this.moduleService = moduleService;
    }

    @PostMapping("add")
    public HttpEntity<?> addModule(@RequestBody ReqModule reqModule){
        return ResponseEntity.ok(moduleService.addModule(reqModule));
    }

    @GetMapping("list/{courseId}")
    public HttpEntity<?> listModule(@PathVariable Long courseId,@RequestParam(required = false) Long page){
        return ResponseEntity.ok(moduleService.getModule(courseId,page));
    }

    @PostMapping("update/{id}")
    public HttpEntity<?> updateModule(@PathVariable Long id,@RequestBody ReqModule reqModule){
        return ResponseEntity.ok(moduleService.updateModule(id,reqModule));
    }

    @PostMapping("delete/{id}")
    public HttpEntity<?> deleteModule(@PathVariable Long id){
        return ResponseEntity.ok(moduleService.deleteModule(id));
    }

}
