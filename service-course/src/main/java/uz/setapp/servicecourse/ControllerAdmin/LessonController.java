package uz.setapp.servicecourse.ControllerAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.payload.ReqLesson;
import uz.setapp.servicecourse.serviceAdmin.LessonService;

@RequestMapping("/admin/lesson")
@RestController
public class LessonController {

    private final LessonService lessonService;

    @Autowired
    public LessonController(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    @PostMapping("add")
    public HttpEntity<?> addLesson(@RequestBody ReqLesson reqLesson){
        return ResponseEntity.ok(lessonService.addLesson(reqLesson));
    }

    @GetMapping("list/{moduleId}")
    public HttpEntity<?> listLesson(@PathVariable Long moduleId,@RequestParam(required = false) Long page){
        return ResponseEntity.ok(lessonService.getLesson(moduleId,page));
    }

    @PostMapping("update/{id}")
    public HttpEntity<?> updateLesson(@PathVariable Long id,@RequestBody ReqLesson reqLesson){
        return ResponseEntity.ok(lessonService.updateLesson(id,reqLesson));
    }

    @PostMapping("delete/{id}")
    public HttpEntity<?> deleteLesson(@PathVariable Long id){
        return ResponseEntity.ok(lessonService.deleteLesson(id));
    }
}
