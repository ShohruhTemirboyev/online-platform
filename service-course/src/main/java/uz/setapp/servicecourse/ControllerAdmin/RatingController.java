package uz.setapp.servicecourse.ControllerAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.payload.ReqRating;
import uz.setapp.servicecourse.serviceAdmin.RatingService;

@RestController
@RequestMapping("/admin/rating")
public class RatingController {

    private final RatingService ratingService;
    @Autowired
    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @PostMapping("add")
    public HttpEntity<?> addRating(@RequestBody ReqRating reqRating){
        return ResponseEntity.ok(ratingService.addRating(reqRating));
    }

    @GetMapping("listTeacher")
    public HttpEntity<?> getRatingTeacher(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(ratingService.getRatingTeacher(page));
    }

    @GetMapping("listLesson")
    public HttpEntity<?> getRatingLesson(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(ratingService.getRatingLesson(page));
    }

    @PostMapping("update/{id}")
    public HttpEntity<?> updateRating(@PathVariable Long id,@RequestBody ReqRating reqRating){
        return ResponseEntity.ok(ratingService.updateRating(id,reqRating));
    }

    @PostMapping("delete/{id}")
    public HttpEntity<?> deleteRating(@PathVariable Long id){
        return ResponseEntity.ok(ratingService.deleteRating(id));
    }
}
