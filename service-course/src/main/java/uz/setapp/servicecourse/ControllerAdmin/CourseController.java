package uz.setapp.servicecourse.ControllerAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.payload.ReqCourse;
import uz.setapp.servicecourse.serviceAdmin.CourseService;

@RestController
@RequestMapping("/admin")
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping("add")
    public HttpEntity<?> addCourse(@RequestBody ReqCourse reqCourse){
        return ResponseEntity.ok(courseService.addCourse(reqCourse));
    }
    @GetMapping("/list")
    public HttpEntity<?> getCourse(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(courseService.getCourse(page));
    }
    @PostMapping("update/{id}")
    public HttpEntity<?> updateCourse(@PathVariable Long id,@RequestBody ReqCourse reqCourse){
        return ResponseEntity.ok(courseService.updateCourse(id,reqCourse));
    }

    @PostMapping("delete/{id}")
    public HttpEntity<?> deleteCourse(@PathVariable Long id){
        return ResponseEntity.ok(courseService.deleteCourse(id));
    }
}
