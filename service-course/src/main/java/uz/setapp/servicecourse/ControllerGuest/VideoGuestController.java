package uz.setapp.servicecourse.ControllerGuest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqCourse;
import uz.setapp.servicecourse.serviceGuest.VideoGuestService;

@RestController
@RequestMapping("/guest/video")
public class VideoGuestController {
    private final VideoGuestService videoGuestService;
    @Autowired
    public VideoGuestController(VideoGuestService videoGuestService) {
        this.videoGuestService = videoGuestService;
    }
    @PostMapping("/add")
    public HttpEntity<?> saveVideo(@RequestParam("file") MultipartFile file) {
        Response response = videoGuestService.addVideo(file);
        return ResponseEntity.ok(response);
    }

}
