package uz.setapp.servicecourse.ControllerGuest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.serviceAdmin.ModuleService;
import uz.setapp.servicecourse.serviceGuest.ModuleGuestService;

@RestController
@RequestMapping("/guest/module")
public class ModuleGuestController {
    private final ModuleGuestService moduleGuestService;

    @Autowired
    public ModuleGuestController(ModuleGuestService moduleGuestService) {
        this.moduleGuestService = moduleGuestService;
    }

    @GetMapping("list/{courseId}")
    public HttpEntity<?> listModule(@PathVariable Long courseId, @RequestParam(required = false) Long page){
        return ResponseEntity.ok(moduleGuestService.getModule(courseId,page));
    }
}
