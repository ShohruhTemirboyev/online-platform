package uz.setapp.servicecourse.ControllerGuest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.setapp.servicecourse.serviceGuest.CourseGuestService;

@RestController
@RequestMapping("/guest")
public class CourseGuestController {
    private final CourseGuestService courseGuestService;
    @Autowired
    public CourseGuestController(CourseGuestService courseGuestService) {
        this.courseGuestService = courseGuestService;
    }
    @GetMapping("/list")
    public HttpEntity<?> getCourse(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(courseGuestService.getCourse(page));
    }

}
