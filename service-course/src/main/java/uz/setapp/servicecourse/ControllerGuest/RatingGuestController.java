package uz.setapp.servicecourse.ControllerGuest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.setapp.servicecourse.serviceAdmin.RatingService;

@RestController
@RequestMapping("/guest/rating")
public class RatingGuestController {
    private final RatingService ratingService;

    @Autowired
    public RatingGuestController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping("listTeacher")
    public HttpEntity<?> getRatingTeacher(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(ratingService.getRatingTeacher(page));
    }

    @GetMapping("listLesson")
    public HttpEntity<?> getRatingLesson(@RequestParam(required = false) Long page){
        return ResponseEntity.ok(ratingService.getRatingLesson(page));
    }
}
