package uz.setapp.servicecourse.ControllerGuest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.setapp.servicecourse.serviceGuest.LessonGuestService;

@RequestMapping("/guest/lesson")
@RestController
public class LessonGuestController {
    private final LessonGuestService lessonGuestService;
    @Autowired
    public LessonGuestController(LessonGuestService lessonGuestService) {
        this.lessonGuestService = lessonGuestService;
    }

    @GetMapping("list/{moduleId}")
    public HttpEntity<?> listLesson(@PathVariable Long moduleId, @RequestParam(required = false) Long page){
        return ResponseEntity.ok(lessonGuestService.getLesson(moduleId,page));
    }
}
