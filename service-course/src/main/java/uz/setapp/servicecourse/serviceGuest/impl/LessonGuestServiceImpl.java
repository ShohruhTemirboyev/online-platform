package uz.setapp.servicecourse.serviceGuest.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.Module;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.repository.LessonRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.ModuleRepository;
import uz.setapp.servicecourse.serviceGuest.LessonGuestService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
@Service
public class LessonGuestServiceImpl implements LessonGuestService {
    private final LessonRepository lessonRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ModuleRepository moduleRepository;
    private final ObjectMapper objectMapper;
    private final MessageRepository messageRepository;

    private final SecurityConfig securityConfig;
    @Autowired
    public LessonGuestServiceImpl(LessonRepository lessonRepository, JdbcTemplate jdbcTemplate, ModuleRepository moduleRepository, ObjectMapper objectMapper, MessageRepository messageRepository, SecurityConfig securityConfig) {
        this.lessonRepository = lessonRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.moduleRepository = moduleRepository;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
        this.securityConfig = securityConfig;
    }

    @Override
    public Response getLesson(Long moduleId, Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        Optional<Module> byId = moduleRepository.findById(moduleId);
        if (byId.isPresent()) {
            if (page != null) {
                Integer count = jdbcTemplate.queryForObject("select count(l.id) from lesson l left join module m on m.id = l.module_id where l.active = true and m.id=?", Integer.class, moduleId);
                Long a = 20 * page;
                List<Map<String, Object>> maps = jdbcTemplate.queryForList("select l.id,l.module_id,l.name,l.first,l.attachment_id from lesson l left join module m on m.id = l.module_id where l.active = true and l.module_id = ? offset ? limit 20",moduleId,a);
                for (int i = 0; i < maps.size(); i++) {
                    Long id = (Long) maps.get(i).get("id");
                    Double ratingSum = jdbcTemplate.queryForObject("select sum(r.rating) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double IsIdCount = jdbcTemplate.queryForObject("select count(r.is_id) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double fullRating = ratingSum / IsIdCount;
                    maps.get(i).put("rating",fullRating);
                }
                objectNode.put("count", count);
                objectNode.putPOJO("data", maps);
                response.setData(objectNode);
                response.setMessage(messageRepository.findByCode(0));
            } else {
                List<Map<String, Object>> maps = jdbcTemplate.queryForList("select l.id,l.module_id,l.name,l.first,l.attachment_id from lesson l left join module m on m.id = l.module_id where l.active = true and l.module_id = ?",moduleId);
                for (int i = 0; i < maps.size(); i++) {
                    Long id = (Long) maps.get(i).get("id");
                    Double ratingSum = jdbcTemplate.queryForObject("select sum(r.rating) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double IsIdCount = jdbcTemplate.queryForObject("select count(r.is_id) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double fullRating = ratingSum / IsIdCount;
                    maps.get(i).put("rating",fullRating);
                }
                response.setData(maps);
                response.setMessage(messageRepository.findByCode(0));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1002));
        }
        return response;
    }
}
