package uz.setapp.servicecourse.serviceGuest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.setapp.servicecourse.entity.Video;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.VideoRepository;
import uz.setapp.servicecourse.serviceGuest.VideoGuestService;
@Service
public class VideoGuestServiceImpl implements VideoGuestService {
    private final VideoRepository videoRepository;
    private final MessageRepository messageRepository;
    @Autowired
    public VideoGuestServiceImpl(VideoRepository videoRepository, MessageRepository messageRepository) {
        this.videoRepository = videoRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public Response addVideo(MultipartFile file) {
        Response response = new Response();
        String docname = file.getOriginalFilename();
        try {
            Video video = new Video(docname, file.getContentType(), file.getBytes());
            Video save = videoRepository.save(video);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
            return response;
        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }
}
