package uz.setapp.servicecourse.serviceGuest;

import org.springframework.web.multipart.MultipartFile;
import uz.setapp.servicecourse.entity.Video;
import uz.setapp.servicecourse.entity.models.Response;

public interface VideoGuestService {
    Response addVideo(MultipartFile file);

}
