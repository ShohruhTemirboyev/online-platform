package uz.setapp.servicecourse.serviceGuest.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.RatingRepository;
import uz.setapp.servicecourse.serviceGuest.RatingGuestService;

import java.util.List;
import java.util.Map;
@Service
public class RatingGuestServiceImpl implements RatingGuestService {
    private final JdbcTemplate jdbcTemplate;

    private final ObjectMapper objectMapper;

    private final MessageRepository messageRepository;

    @Autowired
    public RatingGuestServiceImpl(JdbcTemplate jdbcTemplate, ObjectMapper objectMapper, MessageRepository messageRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
    }



    @Override
    public Response getRatingTeacher(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null){
            Integer count = jdbcTemplate.queryForObject("select count(r.id) from rating r where r.active=true and r.is_teacher=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_teacher=true offset ? limit 20", a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_teacher=true");
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

    @Override
    public Response getRatingLesson(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null){
            Integer count = jdbcTemplate.queryForObject("select count(r.id) from rating r where r.active=true and r.is_lesson=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_lesson=true offset ? limit 20", a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_lesson=true");
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

}
