package uz.setapp.servicecourse.serviceGuest;

import uz.setapp.servicecourse.entity.models.Response;

public interface LessonGuestService {
    Response getLesson(Long moduleId, Long page);
}
