package uz.setapp.servicecourse.serviceGuest;

import uz.setapp.servicecourse.entity.models.Response;

public interface ModuleGuestService {
    Response getModule(Long courseId, Long page);
}
