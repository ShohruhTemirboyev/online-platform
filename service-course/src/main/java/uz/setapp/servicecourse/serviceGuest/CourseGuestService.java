package uz.setapp.servicecourse.serviceGuest;

import uz.setapp.servicecourse.entity.models.Response;

public interface CourseGuestService {
    Response getCourse(Long page);
}
