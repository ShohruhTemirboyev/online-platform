package uz.setapp.servicecourse.serviceGuest;

import uz.setapp.servicecourse.entity.models.Response;

public interface RatingGuestService {
    Response getRatingTeacher(Long page);
    Response getRatingLesson(Long page);
}
