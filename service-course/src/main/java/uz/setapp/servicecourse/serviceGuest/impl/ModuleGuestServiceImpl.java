package uz.setapp.servicecourse.serviceGuest.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.ModuleRepository;
import uz.setapp.servicecourse.serviceGuest.ModuleGuestService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ModuleGuestServiceImpl implements ModuleGuestService {
    private final ModuleRepository moduleRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;
    private final MessageRepository messageRepository;
    private final CourseRepository courseRepository;
    private final SecurityConfig securityConfig;

    @Autowired
    public ModuleGuestServiceImpl(ModuleRepository moduleRepository, JdbcTemplate jdbcTemplate, ObjectMapper objectMapper, MessageRepository messageRepository, CourseRepository courseRepository, SecurityConfig securityConfig) {
        this.moduleRepository = moduleRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
        this.courseRepository = courseRepository;
        this.securityConfig = securityConfig;
    }

    @Override
    public Response getModule(Long courseId, Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        List<Double> list = new ArrayList<>();
        if (page != null) {
            Integer count = jdbcTemplate.queryForObject("select count(m.id) from module m left join course c on c.id = m.course_id where c.id=?", Integer.class, courseId);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select m.id,m.name,m.price,m.description,m.course_id,m.certificate_id,m.mentor_id from module m left join course c on c.id = m.course_id where c.id=? and m.active=true offset ? limit 20", courseId, a);
            for (int i = 0; i < maps.size(); i++) {
                Long id1 = (Long) maps.get(i).get("id");
                Integer countLesson = jdbcTemplate.queryForObject("select count(l.id) from lesson l left join module m on l.module_id = m.id where m.id=?", Integer.class, id1);
                List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select l.id,l.name from lesson l where l.active=true and l.module_id=?", id1);
                for (int j = 0; j < maps1.size(); j++) {
                    Long id = (Long) maps1.get(j).get("id");
                    Double ratingSum = jdbcTemplate.queryForObject("select sum(r.rating) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double IsIdCount = jdbcTemplate.queryForObject("select count(r.is_id) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id);
                    Double fullRating = ratingSum / IsIdCount;
                    list.add(fullRating);
                }
                Double sum = list.stream().mapToDouble(Double::doubleValue).sum();
                Double size = Double.valueOf(list.size());
                Double full = sum / size;
                maps.get(i).put("rating",full);
                Long certificate_id = (Long) maps.get(i).get("certificate_id");
                List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select c.name,c.description,c.attachment_id from certificate c where c.id=?", certificate_id);
                maps.get(i).put("certificate", maps2);
                maps.get(i).put("countLesson",countLesson);
                maps.get(i).put("listLesson",maps1);
            }
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        } else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select m.id,m.name,m.price,m.description,m.course_id,m.mentor_id,(select count(l.id) from lesson l left join module m on l.module_id = m.id where m.id=?) as lesson from module m left join course c on c.id = m.course_id where c.id=? and m.active=true", courseId);
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }
}
