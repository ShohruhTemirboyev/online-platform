package uz.setapp.servicecourse.serviceGuest.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.serviceGuest.CourseGuestService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CourseGuestServiceImpl implements CourseGuestService {
    private final CourseRepository courseRepository;
    private final MessageRepository messageRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public CourseGuestServiceImpl(CourseRepository courseRepository, MessageRepository messageRepository, JdbcTemplate jdbcTemplate, ObjectMapper objectMapper) {
        this.courseRepository = courseRepository;
        this.messageRepository = messageRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
    }

    @Override
    public Response getCourse(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        List<Double> list = new ArrayList<>();
        List<Double> list1 = new ArrayList<>();
        Double full;
        if (page != null) {
            Integer count = jdbcTemplate.queryForObject("select count(c.id) from course c where c.active=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.description,c.photo_id from course c where c.active = true offset ? limit 20", a);
            for (int i = 0; i < maps.size(); i++) {
                Long id = (Long) maps.get(i).get("id");
                Integer countModule = jdbcTemplate.queryForObject("select count(m.id) from module m where m.active = true and m.course_id=?", Integer.class, id);
                Integer countLesson = jdbcTemplate.queryForObject("select count(l.id) from lesson l left join module m on l.module_id = m.id where l.active=true and m.active=true and m.course_id=?", Integer.class, id);
                Double sumModule = jdbcTemplate.queryForObject("select sum(m.price) from module m where m.active = true and m.course_id=?", Double.class, id);
                List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select m.id,m.name,m.price,m.description from module m where m.active=true and m.course_id=?", id);
                for (int i1 = 0; i1 < maps1.size(); i1++) {
                    Long id1 = (Long) maps1.get(i1).get("id");
                    List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select l.id from lesson l where l.active = true and l.module_id = ?", id1);
                    for (int i2 = 0; i2 < maps2.size(); i2++) {
                        Long id2 = (Long) maps2.get(i2).get("id");
                        Double ratingSum = jdbcTemplate.queryForObject("select coalesce(sum(r.rating),0) as summa from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id2);
                        Double IsIdCount = jdbcTemplate.queryForObject("select count(r.is_id) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id2);
                        Double fullRating = ratingSum / IsIdCount;
                        if (!fullRating.isNaN()) {
                            list1.add(fullRating);
                        }
                    }
                    if (!list1.isEmpty()) {
                        Double lessonSum = list1.stream().mapToDouble(Double::doubleValue).sum();
                        Double sizeLesson = Double.valueOf(list1.size());
                        Double fullModule = lessonSum / sizeLesson;
                        list1.clear();
                        if (!fullModule.isNaN()) {
                            list.add(fullModule);
                        }
                    }
                }
                if (!list.isEmpty()){
                    Double moduleSum = list.stream().mapToDouble(Double::doubleValue).sum();
                    Double sizeModule = Double.valueOf(list.size());
                    full = moduleSum / sizeModule;
                    list.clear();
                }else {
                    full=0.0;
                }
                maps.get(i).put("countModule", countModule);
                maps.get(i).put("countLesson", countLesson);
                maps.get(i).put("rating", full);
                maps.get(i).put("priceModule", sumModule);
                maps.get(i).put("listModule", maps1);
            }
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        } else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.description,c.photo_id from course c where c.active = true");
            for (int i = 0; i < maps.size(); i++) {
                Long id = (Long) maps.get(i).get("id");
                Integer countModule = jdbcTemplate.queryForObject("select count(m.id) from module m where m.active = true and m.course_id=?", Integer.class, id);
                Integer countLesson = jdbcTemplate.queryForObject("select count(l.id) from lesson l left join module m on l.module_id = m.id where l.active=true and m.active=true and m.course_id=?", Integer.class, id);
                Double sumModule = jdbcTemplate.queryForObject("select sum(m.price) from module m where m.active = true and m.course_id=?", Double.class, id);
                List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select m.id,m.name,m.price,m.description from module m where m.active=true and m.course_id=?", id);
                for (int i1 = 0; i1 < maps1.size(); i1++) {
                    Long id1 = (Long) maps1.get(i1).get("id");
                    List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select l.id from lesson l where l.active = true and l.module_id = ?", id1);
                    for (int i2 = 0; i2 < maps2.size(); i2++) {
                        Long id2 = (Long) maps2.get(i2).get("id");
                        Double ratingSum = jdbcTemplate.queryForObject("select coalesce(sum(r.rating),0) as summa from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id2);
                        Double IsIdCount = jdbcTemplate.queryForObject("select count(r.is_id) from rating r where r.is_lesson=true and r.is_teacher=false and r.is_id=?", Double.class, id2);
                        Double fullRating = ratingSum / IsIdCount;
                        if (!fullRating.isNaN()) {
                            list1.add(fullRating);
                        }
                    }
                    if (!list1.isEmpty()) {
                        Double lessonSum = list1.stream().mapToDouble(Double::doubleValue).sum();
                        Double sizeLesson = Double.valueOf(list1.size());
                        Double fullModule = lessonSum / sizeLesson;
                        list1.clear();
                        if (!fullModule.isNaN()) {
                            list.add(fullModule);
                        }
                    }
                }
                if (!list.isEmpty()){
                    Double moduleSum = list.stream().mapToDouble(Double::doubleValue).sum();
                    Double sizeModule = Double.valueOf(list.size());
                    full = moduleSum / sizeModule;
                    list.clear();
                }else {
                    full=0.0;
                }
                maps.get(i).put("countModule", countModule);
                maps.get(i).put("countLesson", countLesson);
                maps.get(i).put("rating", full);
                maps.get(i).put("priceModule", sumModule);
                maps.get(i).put("listModule", maps1);
            }
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }
}
