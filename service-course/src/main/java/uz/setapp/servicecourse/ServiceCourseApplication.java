package uz.setapp.servicecourse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceCourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceCourseApplication.class, args);
    }

}
