package uz.setapp.servicecourse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqModule {
    private String name;
    private Double price;
    private String description;
    private Long photoId;
    private Long mentorId;
    private Long courseId;
}
