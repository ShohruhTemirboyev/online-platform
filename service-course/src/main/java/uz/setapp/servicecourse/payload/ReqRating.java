package uz.setapp.servicecourse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqRating {
    private Long id;
    private Double rating;
    private String description;
    private Boolean isTeacher;
    private Boolean isLesson;
    private Long isId;
}
