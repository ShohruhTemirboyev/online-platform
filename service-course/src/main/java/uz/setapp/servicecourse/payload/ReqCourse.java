package uz.setapp.servicecourse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqCourse {
    private Long id;
    private String name;
    private Long photoId;
}
