package uz.setapp.servicecourse.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqLesson {
    private Long lessonId;
    private String name;
    private Boolean first;
    private Long attachmentId;
    private Long moduleId;
}
