package uz.setapp.servicecourse.serviceAdmin;


import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqLesson;

public interface LessonService {
    Response addLesson(ReqLesson reqLesson);
    Response getLesson(Long moduleId,Long page);
    Response updateLesson(Long id,ReqLesson reqLesson);
    Response deleteLesson(Long id);}
