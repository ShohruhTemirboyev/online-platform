package uz.setapp.servicecourse.serviceAdmin;


import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqModule;

public interface ModuleService {
    Response addModule(ReqModule reqModule);
    Response getModule(Long courseId,Long page);
    Response updateModule(Long id,ReqModule reqModule);
    Response deleteModule(Long id);
}
