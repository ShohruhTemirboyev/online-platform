package uz.setapp.servicecourse.serviceAdmin.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.Rating;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqRating;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.RatingRepository;
import uz.setapp.servicecourse.serviceAdmin.RatingService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RatingServiceImpl implements RatingService {

    private final JdbcTemplate jdbcTemplate;

    private final ObjectMapper objectMapper;

    private final MessageRepository messageRepository;

    private final CourseRepository courseRepository;

    private final SecurityConfig securityConfig;

    private final RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(JdbcTemplate jdbcTemplate, ObjectMapper objectMapper, MessageRepository messageRepository, CourseRepository courseRepository, SecurityConfig securityConfig, RatingRepository ratingRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
        this.courseRepository = courseRepository;
        this.securityConfig = securityConfig;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public Response addRating(ReqRating reqRating) {
        Response response = new Response();
        Rating rating = new Rating();
        rating.setRating(reqRating.getRating());
        rating.setDescription(reqRating.getDescription());
        rating.setIsTeacher(reqRating.getIsTeacher());
        rating.setIsLesson(reqRating.getIsLesson());
        rating.setIsId(reqRating.getIsId());
        Rating save = ratingRepository.save(rating);
        response.setData(save.getId());
        response.setMessage(messageRepository.findByCode(0));
        return response;
    }

    @Override
    public Response getRatingTeacher(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null){
            Integer count = jdbcTemplate.queryForObject("select count(r.id) from rating r where r.active=true and r.is_teacher=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_teacher=true offset ? limit 20", a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_teacher=true");
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

    @Override
    public Response getRatingLesson(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null){
            Integer count = jdbcTemplate.queryForObject("select count(r.id) from rating r where r.active=true and r.is_lesson=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_lesson=true offset ? limit 20", a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select r.id,r.rating,r.description from rating r where r.active=true and r.is_lesson=true");
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

    @Override
    public Response updateRating(Long id, ReqRating reqRating) {
        Response response = new Response();
        Optional<Rating> byId = ratingRepository.findById(id);
        if (byId.isPresent()) {
            Rating rating = byId.get();
            rating.setRating(reqRating.getRating());
            rating.setDescription(reqRating.getDescription());
            rating.setIsTeacher(reqRating.getIsTeacher());
            rating.setIsLesson(reqRating.getIsLesson());
            rating.setIsId(reqRating.getIsId());
            Rating save = ratingRepository.save(rating);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(1007));
        }
        return response;
    }

    @Override
    public Response deleteRating(Long id) {
        Response response = new Response();
        Optional<Rating> byId = ratingRepository.findById(id);
        if (byId.isPresent()) {
            Rating rating = byId.get();
            rating.setActive(false);
            Rating save = ratingRepository.save(rating);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(1007));
        }
        return response;
    }
}
