package uz.setapp.servicecourse.serviceAdmin;


import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqCourse;

public interface CourseService {
    Response addCourse(ReqCourse reqCourse);
    Response getCourse(Long page);
    Response updateCourse(Long id,ReqCourse reqCourse);
    Response deleteCourse(Long id);

}
