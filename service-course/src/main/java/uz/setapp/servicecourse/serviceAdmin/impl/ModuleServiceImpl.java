package uz.setapp.servicecourse.serviceAdmin.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.Course;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqModule;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.ModuleRepository;
import uz.setapp.servicecourse.serviceAdmin.ModuleService;
import uz.setapp.servicecourse.entity.Module;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ModuleServiceImpl implements ModuleService {
    private final ModuleRepository moduleRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;
    private final MessageRepository messageRepository;
    private final CourseRepository courseRepository;

    private final SecurityConfig securityConfig;

    @Autowired
    public ModuleServiceImpl(ModuleRepository moduleRepository, MessageRepository messageRepository, CourseRepository courseRepository, JdbcTemplate jdbcTemplate, ObjectMapper objectMapper, SecurityConfig securityConfig) {
        this.moduleRepository = moduleRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
        this.courseRepository = courseRepository;
        this.securityConfig = securityConfig;
    }

    RestTemplate restTemplate = new RestTemplate();

    String mentorUrl = "http://localhost:8088/mentor/api/mentor/find/";
    String photoUrl = "http://localhost:8088/photo/api/photo/find/";

    @Override
    public Response addModule(ReqModule reqModule) {
        Response response = new Response();
        Optional<Course> byId = courseRepository.findById(reqModule.getCourseId());
        if (byId.isPresent()) {
            String token = securityConfig.requestUrl().getHeader("token");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<String> entity = new HttpEntity<>("body", headers);
            ResponseEntity<Boolean> exchange = restTemplate.exchange(mentorUrl + reqModule.getMentorId(), HttpMethod.GET, entity, boolean.class);
            if (Boolean.TRUE.equals(exchange.getBody())) {
                String token1 = securityConfig.requestUrl().getHeader("token");
                HttpHeaders headers1 = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "Bearer " + token);
                HttpEntity<String> entity1 = new HttpEntity<>("body", headers);
                ResponseEntity<Boolean> exchange1 = restTemplate.exchange(photoUrl + reqModule.getPhotoId(), HttpMethod.GET, entity1, boolean.class);
                if (Boolean.TRUE.equals(exchange.getBody())) {
                    Module module = new Module();
                    module.setName(reqModule.getName());
                    module.setPrice(reqModule.getPrice());
                    module.setDescription(reqModule.getDescription());
                    module.setPhotoId(reqModule.getPhotoId());
                    module.setMentorId(reqModule.getMentorId());
                    module.setCourseId(reqModule.getCourseId());
                    Module save = moduleRepository.save(module);
                    response.setData(save.getId());
                    response.setMessage(messageRepository.findByCode(0));
                }else {
                    response.setMessage(messageRepository.findByCode(1008));
                }
            } else {
                response.setMessage(messageRepository.findByCode(1005));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1001));
        }

        return response;
    }

    @Override
    public Response getModule(Long courseId, Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null) {
            Integer count = jdbcTemplate.queryForObject("select count(m.id) from module m left join course c on c.id = m.course_id where c.id=?", Integer.class, courseId);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.photo_id,m.id as module_id,m.name as module_name,m.price,m.photo_id as module_photo_id from module m left join course c on c.id = m.course_id where c.id=? and m.active=true offset ? limit 20", courseId, a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        } else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.photo_id,m.id as module_id,m.name as module_name,m.price,m.photo_id as module_photo_id from module m left join course c on c.id = m.course_id where c.id=? and m.active=true", courseId);
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

    @Override
    public Response updateModule(Long id, ReqModule reqModule) {
        Response response = new Response();
        Optional<Course> byId1 = courseRepository.findById(reqModule.getCourseId());
        if (byId1.isPresent()) {
            Optional<Module> byId = moduleRepository.findById(id);
            if (byId.isPresent()) {
                String token = securityConfig.requestUrl().getHeader("token");
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "Bearer " + token);
                HttpEntity<String> entity = new HttpEntity<>("body", headers);
                ResponseEntity<Boolean> exchange = restTemplate.exchange(mentorUrl + reqModule.getMentorId(), HttpMethod.GET, entity, boolean.class);
                if (Boolean.TRUE.equals(exchange.getBody())) {
                    Module module = byId.get();
                    module.setName(reqModule.getName());
                    module.setPrice(reqModule.getPrice());
                    module.setDescription(reqModule.getDescription());
                    module.setPhotoId(reqModule.getPhotoId());
                    module.setMentorId(reqModule.getMentorId());
                    module.setCourseId(reqModule.getCourseId());
                    Module save = moduleRepository.save(module);
                    response.setData(save.getId());
                    response.setMessage(messageRepository.findByCode(0));
                } else {
                    response.setMessage(messageRepository.findByCode(1005));
                }
            } else {
                response.setMessage(messageRepository.findByCode(1002));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1001));
        }

        return response;
    }

    @Override
    public Response deleteModule(Long id) {
        Response response = new Response();
        Optional<Module> byId = moduleRepository.findById(id);
        if (byId.isPresent()) {
            Module module = byId.get();
            module.setActive(false);
            Module save = moduleRepository.save(module);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        } else {
            response.setMessage(messageRepository.findByCode(1002));
        }
        return response;
    }
}
