package uz.setapp.servicecourse.serviceAdmin.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.setapp.servicecourse.entity.Course;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqCourse;
import uz.setapp.servicecourse.repository.CourseRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.serviceAdmin.CourseService;


import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final MessageRepository messageRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, MessageRepository messageRepository, JdbcTemplate jdbcTemplate, ObjectMapper objectMapper) {
        this.courseRepository = courseRepository;
        this.messageRepository = messageRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.objectMapper = objectMapper;
    }


    @Override
    public Response addCourse(ReqCourse reqCourse) {
        Response response = new Response();
        Course course = new Course();
        course.setName(reqCourse.getName());
        course.setPhotoId(reqCourse.getPhotoId());
        Course save = courseRepository.save(course);
        response.setData(save.getId());
        response.setMessage(messageRepository.findByCode(0));
        return response;
    }

    @Override
    public Response getCourse(Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        if (page != null){
            Integer count = jdbcTemplate.queryForObject("select count(c.id) from course c where c.active=true", Integer.class);
            Long a = 20 * page;
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.photo_id from course c where c.active = true offset ? limit 20",a);
            objectNode.put("count", count);
            objectNode.putPOJO("data", maps);
            response.setData(objectNode);
            response.setMessage(messageRepository.findByCode(0));
        }else {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id,c.name,c.photo_id from course c where c.active = true");
            response.setData(maps);
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }

    @Override
    public Response updateCourse(Long id, ReqCourse reqCourse) {
    Response response = new Response();
        Optional<Course> byId = courseRepository.findById(id);
        if (byId.isPresent()) {
            Course course = byId.get();
            course.setName(reqCourse.getName());
            course.setPhotoId(reqCourse.getPhotoId());
            Course save = courseRepository.save(course);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        }else {
            response.setMessage(messageRepository.findByCode(1001));
        }
        return response;
    }

    @Override
    public Response deleteCourse(Long id) {
    Response response = new Response();
        Optional<Course> byId = courseRepository.findById(id);
        if (byId.isPresent()) {
            Course course = byId.get();
            course.setActive(false);
            Course save = courseRepository.save(course);
            response.setData(course.getId());
            response.setMessage(messageRepository.findByCode(0));
        }
        return response;
    }
}
