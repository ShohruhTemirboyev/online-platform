package uz.setapp.servicecourse.serviceAdmin.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.setapp.servicecourse.config.SecurityConfig;
import uz.setapp.servicecourse.entity.Lesson;
import uz.setapp.servicecourse.entity.Module;
import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqLesson;
import uz.setapp.servicecourse.repository.LessonRepository;
import uz.setapp.servicecourse.repository.MessageRepository;
import uz.setapp.servicecourse.repository.ModuleRepository;
import uz.setapp.servicecourse.serviceAdmin.LessonService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;
    private final JdbcTemplate jdbcTemplate;
    private final ModuleRepository moduleRepository;
    private final ObjectMapper objectMapper;
    private final MessageRepository messageRepository;

    private final SecurityConfig securityConfig;

    @Autowired
    public LessonServiceImpl(LessonRepository lessonRepository, JdbcTemplate jdbcTemplate, ModuleRepository moduleRepository, ObjectMapper objectMapper, MessageRepository messageRepository, SecurityConfig securityConfig) {
        this.lessonRepository = lessonRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.moduleRepository = moduleRepository;
        this.objectMapper = objectMapper;
        this.messageRepository = messageRepository;
        this.securityConfig = securityConfig;
    }

    RestTemplate restTemplate = new RestTemplate();

    String attachmentUrl = "http://localhost:8088/attachment/api/attachment/find/";

    @Override
    public Response addLesson(ReqLesson reqLesson) {
        Response response = new Response();
        Optional<Module> byId = moduleRepository.findById(reqLesson.getModuleId());
        if (byId.isPresent()) {
            String token = securityConfig.requestUrl().getHeader("token");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<String> entity = new HttpEntity<>("body", headers);
            ResponseEntity<Boolean> exchange = restTemplate.exchange(attachmentUrl + reqLesson.getAttachmentId(), HttpMethod.GET, entity, boolean.class);
            if (Boolean.TRUE.equals(exchange.getBody())) {
                Lesson lesson = new Lesson();
                lesson.setName(reqLesson.getName());
                lesson.setFirst(false);
                lesson.setAttachmentId(reqLesson.getAttachmentId());
                lesson.setModuleId(reqLesson.getModuleId());
                Lesson save = lessonRepository.save(lesson);
                response.setData(save.getId());
                response.setMessage(messageRepository.findByCode(0));
            } else {
                response.setMessage(messageRepository.findByCode(1006));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1002));
        }
        return response;
    }

    @Override
    public Response getLesson(Long moduleId, Long page) {
        Response response = new Response();
        ObjectNode objectNode = objectMapper.createObjectNode();
        Optional<Module> byId = moduleRepository.findById(moduleId);
        if (byId.isPresent()) {
            if (page != null) {
                Integer count = jdbcTemplate.queryForObject("select count(l.id) from lesson l left join module m on m.id = l.module_id where l.active = true and m.id=?", Integer.class, moduleId);
                Long a = 20 * page;
                List<Map<String, Object>> maps = jdbcTemplate.queryForList("select m.id, l.id as lesson_id,l.name,l.first,l.attachment_id from lesson l left join module m on m.id = l.module_id where l.active = true and m.id=? offset ? limit 20", moduleId, a);
                objectNode.put("count", count);
                objectNode.putPOJO("data", maps);
                response.setData(objectNode);
                response.setMessage(messageRepository.findByCode(0));
            } else {
                List<Map<String, Object>> maps = jdbcTemplate.queryForList("select m.id, l.id as lesson_id,l.name,l.first,l.attachment_id from lesson l left join module m on m.id = l.module_id where l.active = true and m.id=?", moduleId);
                response.setData(maps);
                response.setMessage(messageRepository.findByCode(0));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1002));
        }
        return response;
    }

    @Override
    public Response updateLesson(Long id, ReqLesson reqLesson) {
        Response response = new Response();
        Optional<Lesson> byId = lessonRepository.findById(id);
        if (byId.isPresent()) {
            String token = securityConfig.requestUrl().getHeader("token");
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            HttpEntity<String> entity = new HttpEntity<>("body", headers);
            ResponseEntity<Boolean> exchange = restTemplate.exchange(attachmentUrl + reqLesson.getAttachmentId(), HttpMethod.GET, entity, boolean.class);

            if (Boolean.TRUE.equals(exchange.getBody())) {
                Lesson lesson = byId.get();
                lesson.setName(reqLesson.getName());
                lesson.setFirst(reqLesson.getFirst());
                lesson.setAttachmentId(reqLesson.getAttachmentId());
                Optional<Module> byId1 = moduleRepository.findById(reqLesson.getModuleId());
                if (byId1.isPresent()) {
                    lesson.setModuleId(reqLesson.getModuleId());
                } else {
                    response.setMessage(messageRepository.findByCode(1002));
                }
                Lesson save = lessonRepository.save(lesson);
                response.setData(save.getId());
                response.setMessage(messageRepository.findByCode(0));
            } else {
                response.setMessage(messageRepository.findByCode(1006));
            }
        }
        return response;
    }

    @Override
    public Response deleteLesson(Long id) {
        Response response = new Response();
        Optional<Lesson> byId = lessonRepository.findById(id);
        if (byId.isPresent()) {
            Lesson lesson = byId.get();
            lesson.setActive(false);
            Lesson save = lessonRepository.save(lesson);
            response.setData(save.getId());
            response.setMessage(messageRepository.findByCode(0));
        } else {
            response.setMessage(messageRepository.findByCode(1003));
        }
        return response;
    }
}
