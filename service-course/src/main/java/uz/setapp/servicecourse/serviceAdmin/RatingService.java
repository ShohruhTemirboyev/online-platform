package uz.setapp.servicecourse.serviceAdmin;

import uz.setapp.servicecourse.entity.models.Response;
import uz.setapp.servicecourse.payload.ReqRating;

public interface RatingService {
    Response addRating(ReqRating reqRating);
    Response getRatingTeacher(Long page);
    Response getRatingLesson(Long page);
    Response updateRating(Long id, ReqRating reqRating);
    Response deleteRating(Long id);
}
