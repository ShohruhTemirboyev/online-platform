package uz.setapp.servicephoto.service;


import org.springframework.web.multipart.MultipartFile;
import uz.setapp.servicephoto.entity.models.Response;

public interface PhotoService {
    Response savePhoto(MultipartFile file);

    Response updatePhoto(Long id, MultipartFile file);

    Response deletePhoto(Long id);
    Response getPhoto(Long id);
    boolean findPhoto(Long id);

}
