package uz.setapp.servicephoto.service.impl;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.setapp.servicephoto.entity.Photo;
import uz.setapp.servicephoto.entity.PhotoContent;
import uz.setapp.servicephoto.entity.models.Response;
import uz.setapp.servicephoto.repository.MessageRepository;
import uz.setapp.servicephoto.repository.PhotoContentRepository;
import uz.setapp.servicephoto.repository.PhotoRepository;
import uz.setapp.servicephoto.service.PhotoService;

import java.util.Optional;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private final PhotoContentRepository photoContentRepository;
    private final JdbcTemplate jdbcTemplate;
    private final MessageRepository messageRepository;

    public PhotoServiceImpl(PhotoRepository photoRepository,
                            PhotoContentRepository photoContentRepository,
                            JdbcTemplate jdbcTemplate,
                            MessageRepository messageRepository) {
        this.photoRepository = photoRepository;
        this.photoContentRepository = photoContentRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.messageRepository = messageRepository;
    }

    public Response savePhoto(MultipartFile file) {
        Response response = new Response();
        Photo photo = new Photo();
        photo.setName(file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf(".")));
        photo.setTypeContent(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf(".")+1));
        photo.setType(file.getContentType());
        photo.setSize(file.getSize());
        PhotoContent photoContent = new PhotoContent();
        try {
            photoContent.setBytes(file.getBytes());
            Photo save = photoRepository.save(photo);
            photoContent.setPhoto(save);
            photoContentRepository.save(photoContent);
            response.setMessage(messageRepository.findByCode(0));

        } catch (Exception e) {
            response.setMessage(messageRepository.findByCode(1));
        }
        return response;

    }

    @Override
    public Response updatePhoto(Long id, MultipartFile file) {
        Response response = new Response();
        Optional<Photo> byId = photoRepository.findById(id);
        if (byId.isPresent()) {
            Photo photo = byId.get();
            photo.setName(file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf(".")));
            photo.setType(file.getContentType());
            photo.setTypeContent(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf(".")+1));
            photo.setSize(file.getSize());
            Optional<PhotoContent> byId1 = photoContentRepository.findById(id);
            try {
                PhotoContent photoContent = byId1.get();
                photoContent.setBytes(file.getBytes());
                Photo save = photoRepository.save(photo);
                photoContent.setPhoto(save);
                photoContentRepository.save(photoContent);
                response.setMessage(messageRepository.findByCode(0));

            } catch (Exception e) {
                response.setMessage(messageRepository.findByCode(1));
            }
        } else {
            response.setMessage(messageRepository.findByCode(1004));
        }

        return response;

    }

    @Override
    public Response deletePhoto(Long id) {
        Response response = new Response();
        Optional<Photo> byId = photoRepository.findById(id);
        if (byId.isPresent()) {
            try {
                photoContentRepository.deleteById(id);
                photoRepository.deleteById(id);
                response.setMessage(messageRepository.findByCode(0));
            } catch (Exception e) {
                response.setMessage(messageRepository.findByCode(1));
            }

        } else {
            response.setMessage(messageRepository.findByCode(1004));
        }
        return response;
    }


    @Override
    public Response getPhoto(Long id) {
        Response response = new Response();
        Optional<PhotoContent> byId = photoContentRepository.findById(id);
        PhotoContent photoContent = byId.get();
        if (byId.isPresent()) {
            response.setData(byId.get());
            response.setMessage(messageRepository.findByCode(0));
        } else {
            response.setData(photoContent.getId());
            response.setMessage(messageRepository.findByCode(1004));
        }
        return response;

    }

    @Override
    public boolean findPhoto(Long id) {
        Optional<Photo> byId = photoRepository.findById(id);
        return byId.isPresent();
    }


}
