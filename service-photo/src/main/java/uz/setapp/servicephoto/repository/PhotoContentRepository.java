package uz.setapp.servicephoto.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicephoto.entity.PhotoContent;

@Repository
public interface PhotoContentRepository  extends JpaRepository<PhotoContent, Long> {
}
