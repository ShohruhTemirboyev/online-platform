package uz.setapp.servicephoto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicephoto.entity.Photo;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Long> {

}
