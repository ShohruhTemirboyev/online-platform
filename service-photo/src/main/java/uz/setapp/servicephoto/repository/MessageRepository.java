package uz.setapp.servicephoto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.servicephoto.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {

    Message findByCode(Integer code);
}
