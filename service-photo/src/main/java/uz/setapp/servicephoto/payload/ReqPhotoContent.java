package uz.setapp.servicephoto.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPhotoContent {

    private byte[] bytes;


}
