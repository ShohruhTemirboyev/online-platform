package uz.setapp.servicephoto.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPhoto {

    public String name;
    public String type;
    public String typeContent;
    public Long size;

}
