package uz.setapp.servicephoto.controller;


import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import uz.setapp.servicephoto.entity.models.Response;
import uz.setapp.servicephoto.service.PhotoService;

@RestController
@RequestMapping("/api/photo")
public class PhotoController {
    private final PhotoService photoService;

    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @PostMapping("/add")
    public HttpEntity<?> saveAttachment(@RequestParam("file") MultipartFile file) {
        Response response = photoService.savePhoto(file);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get/{id}")
    public HttpEntity<?> getAttachment(@PathVariable Long id) {
        Response response = photoService.getPhoto(id);
        return ResponseEntity.ok(response);

    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> updateAttachment(@PathVariable Long id,@RequestParam("file") MultipartFile file) {
        Response response = photoService.updatePhoto(id,file);
        return ResponseEntity.ok(response);

    }

    @PostMapping("/delete/{id}")
    public HttpEntity<?> deleteAttachment(@PathVariable Long id) {
        Response response = photoService.deletePhoto(id);
        return ResponseEntity.ok(response);

    }
    @GetMapping("/find/{id}")
    public HttpEntity<?> findAttachment(@PathVariable Long id) {
        boolean response = photoService.findPhoto(id);
        return ResponseEntity.ok(response);

    }
}
