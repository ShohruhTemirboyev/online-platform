package uz.setapp.gateway.service.dto.enam;


public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_SUPER_ADMIN
}
