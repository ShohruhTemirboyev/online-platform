package uz.setapp.service.attachment.controller;


import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.setapp.service.attachment.entity.models.Response;
import uz.setapp.service.attachment.service.AttachmentService;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {
    private final AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping("/add")
    public HttpEntity<?> saveAttachment(@RequestParam("file") MultipartFile file) {
        Response response = attachmentService.saveAttachment(file);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get/{id}")
    public HttpEntity<?> getAttachment(@PathVariable Long id) {
        Response response = attachmentService.getAttachment(id);
        return ResponseEntity.ok(response);

    }

    @PostMapping("/update/{id}")
    public HttpEntity<?> updateAttachment(@PathVariable Long id,@RequestParam("file") MultipartFile file) {
        Response response = attachmentService.updateAttachment(id,file);
        return ResponseEntity.ok(response);

    }

    @PostMapping("/delete/{id}")
    public HttpEntity<?> deleteAttachment(@PathVariable Long id) {
        Response response = attachmentService.deleteAttachment(id);
        return ResponseEntity.ok(response);

    }
    @GetMapping("/find/{id}")
    public HttpEntity<?> findAttachment(@PathVariable Long id) {
        boolean response = attachmentService.findAttachment(id);
        return ResponseEntity.ok(response);

    }
}
