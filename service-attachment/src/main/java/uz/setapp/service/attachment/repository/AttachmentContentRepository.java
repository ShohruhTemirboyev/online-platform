package uz.setapp.service.attachment.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.service.attachment.entity.AttachmentContent;

@Repository
public interface AttachmentContentRepository  extends JpaRepository<AttachmentContent, Long> {
}
