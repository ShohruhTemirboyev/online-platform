package uz.setapp.service.attachment.service.impl;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.setapp.service.attachment.repository.AttachmentRepository;
import uz.setapp.service.attachment.service.AttachmentService;
import uz.setapp.service.attachment.entity.Attachment;
import uz.setapp.service.attachment.entity.AttachmentContent;
import uz.setapp.service.attachment.entity.models.Response;
import uz.setapp.service.attachment.repository.AttachmentContentRepository;
import uz.setapp.service.attachment.repository.MessageRepository;

import java.util.Optional;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final JdbcTemplate jdbcTemplate;

    private final MessageRepository messageRepository;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository,
                                 AttachmentContentRepository attachmentContentRepository,
                                 JdbcTemplate jdbcTemplate,
                                 MessageRepository messageRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.messageRepository = messageRepository;
    }

    public Response saveAttachment(MultipartFile file) {
        Response response = new Response();
        Attachment attachment = new Attachment();
        attachment.setName(file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf(".")));
        attachment.setType(file.getContentType());
        attachment.setTypeContent(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf(".")+1));
        attachment.setSize(file.getSize());
        AttachmentContent attachmentContent = new AttachmentContent();
        try {
            attachmentContent.setBytes(file.getBytes());
            Attachment save = attachmentRepository.save(attachment);
            attachmentContent.setAttachment(save);
            attachmentContentRepository.save(attachmentContent);
            response.setMessage(messageRepository.findByCode(0));

        } catch (Exception e) {
            response.setMessage(messageRepository.findByCode(1));
        }
        return response;

    }

    @Override
    public Response updateAttachment(Long id, MultipartFile file) {
        Response response = new Response();
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (byId.isPresent()) {
            Attachment attachment = byId.get();
            attachment.setName(file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf(".")));
            attachment.setType(file.getContentType());
            attachment.setTypeContent(file.getOriginalFilename().substring(file.getOriginalFilename().indexOf(".")+1));
            attachment.setSize(file.getSize());
            Optional<AttachmentContent> byId1 = attachmentContentRepository.findById(id);
            try {
                AttachmentContent attachmentContent = byId1.get();
                attachmentContent.setBytes(file.getBytes());
                Attachment save = attachmentRepository.save(attachment);
                attachmentContent.setAttachment(save);
                attachmentContentRepository.save(attachmentContent);
                response.setMessage(messageRepository.findByCode(0));

            } catch (Exception e) {
                response.setMessage(messageRepository.findByCode(1));
            }
        }else {
            response.setMessage(messageRepository.findByCode(1004));
        }

        return response;

    }

    @Override
    public Response deleteAttachment(Long id) {
        Response response = new Response();
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (byId.isPresent()) {
            try {
                attachmentContentRepository.deleteById(id);
                attachmentRepository.deleteById(id);
                response.setMessage(messageRepository.findByCode(0));
            } catch (Exception e) {
                response.setMessage(messageRepository.findByCode(1));
            }

        } else {
            response.setMessage(messageRepository.findByCode(1004));
        }
        return response;
    }


    @Override
    public Response getAttachment(Long id) {
        Response response = new Response();
        Optional<AttachmentContent> byId = attachmentContentRepository.findById(id);
        AttachmentContent attachmentContent = byId.get();
        if (byId.isPresent()){
            response.setData(byId.get());
            response.setMessage(messageRepository.findByCode(0));
        }else {
          response.setData(attachmentContent.getId());
          response.setMessage(messageRepository.findByCode(1004));
        }
        return response;

    }

    @Override
    public boolean findAttachment(Long id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        return  byId.isPresent();
    }


}
