package uz.setapp.service.attachment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.setapp.service.attachment.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {

    Message findByCode(Integer code);
}
