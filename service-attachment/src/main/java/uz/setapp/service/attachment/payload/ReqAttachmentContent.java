package uz.setapp.service.attachment.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqAttachmentContent {

    @NotBlank
    private byte[] bytes;


}
