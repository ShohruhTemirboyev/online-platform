package uz.setapp.service.attachment.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqAttachment {

    @NotBlank
    public String name;
    @NotBlank
    public String type;
    @NotBlank
    public String typeContent;
    @NotBlank
    public Long size;

}
