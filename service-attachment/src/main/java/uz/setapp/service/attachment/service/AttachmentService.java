package uz.setapp.service.attachment.service;


import org.springframework.web.multipart.MultipartFile;
import uz.setapp.service.attachment.entity.models.Response;

public interface AttachmentService {
    Response saveAttachment(MultipartFile file);

    Response updateAttachment(Long id, MultipartFile file);

    Response deleteAttachment(Long id);
    Response getAttachment(Long id);
    boolean findAttachment(Long id);

}
