package uz.setapp.service.attachment.entity.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.setapp.service.attachment.entity.Message;
import uz.setapp.service.attachment.payload.ResMessage;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    private Object data;
    private ResMessage status;

    public Response(Object data, Message status) {
        this.data = data;
        this.status = new ResMessage(status.getCode(),status.getMessage(),status.getMessage_ru(),status.getMessage_uz());
    }

    public void setData(Object data) {
        this.data = data;
    }
    public void setMessage(Message message) {
        this.status = new ResMessage(message.getCode(),message.getMessage(),message.getMessage_ru(),message.getMessage_uz());
    }
}
